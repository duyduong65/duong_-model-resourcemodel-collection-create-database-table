<?php

namespace Add\Information\Controller\Findbyid;

use Magento\Framework\App\Action\Context;

class Id extends \Magento\Framework\App\Action\Action
{
    protected $_userFactory;
    protected $_pageFactory;

    public function __construct(
        Context $context,
        \Add\Information\Model\UsersFactory $usersFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
        $this->_userFactory = $usersFactory;
    }

    public function execute()
    {
       return $this->_pageFactory->create();
    }
}
