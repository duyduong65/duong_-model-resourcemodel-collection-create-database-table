<?php

namespace Add\Information\Model\ResourceModel;

class Users extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Custom method
     */
    public function _construct()
    {
        $this->_init('internship', 'id');
    }
}
